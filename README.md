# hidcat

[![build status](https://ci.gitlab.com/projects/1/status.svg?ref=master)](https://ci.gitlab.com/projects/1?ref=master)

## Running

Setup rust, likely using [rustup](https://rustup.rs/). Switch to an \*available [Virtual Console](https://www.google.com/search?q=switching+virtual+console+-nintendo) and login. Change to the directory of hidcat source and `cargo run <host>` or `cargo run <user>@<host>`.

\* Not already running Wayland or X11

## Using

To enter autistic mode, hit Enter followed by shift-\` or `~`. Then function keys to change vt, `.` to exit, or `~` again to send `~`. Afterward you'll be out of autistic mode, but a simple shift-\` will re-enter... No need to hit Enter again.

## Installation

Not yet
