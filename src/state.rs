use keycode::KeyboardState;

/// State of a Key
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum KeyState {
    /// Key is pressed
    Pressed,
    /// Key is released
    Released,
}

impl From<input::event::keyboard::KeyState> for KeyState {
    fn from(val: input::event::keyboard::KeyState) -> Self {
        match val {
            input::event::keyboard::KeyState::Pressed => KeyState::Pressed,
            input::event::keyboard::KeyState::Released => KeyState::Released,
        }
    }
}

impl From<keycode::KeyState> for KeyState {
    fn from(val: keycode::KeyState) -> Self {
        match val {
            keycode::KeyState::Pressed => KeyState::Pressed,
            keycode::KeyState::Released => KeyState::Released,
        }
    }
}

impl From<KeyState> for input::event::keyboard::KeyState {
    fn from(val: KeyState) -> Self {
        match val {
            KeyState::Pressed => input::event::keyboard::KeyState::Pressed,
            KeyState::Released => input::event::keyboard::KeyState::Released,
        }
    }
}

impl From<KeyState> for keycode::KeyState {
    fn from(val: KeyState) -> Self {
        match val {
            KeyState::Pressed => keycode::KeyState::Pressed,
            KeyState::Released => keycode::KeyState::Released,
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Factory {
    Initial(StateMachine<EitherShift, Initial>),
    PostEnter(StateMachine<EitherShift, PostEnter>),
    ShiftPostEnter(StateMachine<Shift, PostEnter>),
    UnShiftPostEnter(StateMachine<UnShift, PostEnter>),
    PostTilde(StateMachine<EitherShift, PostTilde>),
    Exit(StateMachine<EitherShift, Exit>),
    SwitchTo(StateMachine<EitherShift, SwitchTo>),
    SwitchToNext(StateMachine<EitherShift, SwitchToNext>),
    SwitchToPrevious(StateMachine<EitherShift, SwitchToPrevious>),
}

#[derive(Debug, PartialEq)]
pub struct Shift;
#[derive(Debug, PartialEq)]
pub struct UnShift;
#[derive(Debug, PartialEq)]
pub struct EitherShift;
pub trait ShiftState {}
impl ShiftState for Shift {}
impl ShiftState for UnShift {}
impl ShiftState for EitherShift {}

#[derive(Debug, PartialEq)]
pub struct Initial;
#[derive(Debug, PartialEq)]
pub struct PostEnter;
#[derive(Debug, PartialEq)]
pub struct PostTilde;
#[derive(Debug, PartialEq)]
pub struct Exit;
#[derive(Debug, PartialEq)]
pub struct SwitchTo(pub i32);
#[derive(Debug, PartialEq)]
pub struct SwitchToNext;
#[derive(Debug, PartialEq)]
pub struct SwitchToPrevious;
pub trait State {}
impl State for Initial {}
impl State for PostEnter {}
impl State for PostTilde {}
impl State for Exit {}
impl State for SwitchTo {}
impl State for SwitchToNext {}
impl State for SwitchToPrevious {}

#[derive(Debug, PartialEq)]
pub struct StateMachine<SHIFT: ShiftState, STATE: State> {
    keyboard_state: KeyboardState,
    shift: SHIFT,
    pub state: STATE,
}

impl Default for Factory {
    fn default() -> Self {
        Self::Initial(StateMachine {
            keyboard_state: KeyboardState::new(None),
            shift: EitherShift,
            state: Initial,
        })
    }
}

impl From<StateMachine<EitherShift, Initial>> for StateMachine<EitherShift, PostEnter> {
    fn from(val: StateMachine<EitherShift, Initial>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: PostEnter,
        }
    }
}

impl From<StateMachine<EitherShift, PostEnter>> for StateMachine<Shift, PostEnter> {
    fn from(val: StateMachine<EitherShift, PostEnter>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: Shift,
            state: PostEnter,
        }
    }
}

impl From<StateMachine<EitherShift, PostEnter>> for StateMachine<UnShift, PostEnter> {
    fn from(val: StateMachine<EitherShift, PostEnter>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: UnShift,
            state: PostEnter,
        }
    }
}

impl From<StateMachine<UnShift, PostEnter>> for StateMachine<Shift, PostEnter> {
    fn from(val: StateMachine<UnShift, PostEnter>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: Shift,
            state: PostEnter,
        }
    }
}

impl From<StateMachine<Shift, PostEnter>> for StateMachine<UnShift, PostEnter> {
    fn from(val: StateMachine<Shift, PostEnter>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: UnShift,
            state: PostEnter,
        }
    }
}

impl From<StateMachine<EitherShift, PostEnter>> for StateMachine<EitherShift, Initial> {
    fn from(val: StateMachine<EitherShift, PostEnter>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: Initial,
        }
    }
}

impl From<StateMachine<UnShift, PostEnter>> for StateMachine<EitherShift, Initial> {
    fn from(val: StateMachine<UnShift, PostEnter>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: Initial,
        }
    }
}

impl From<StateMachine<Shift, PostEnter>> for StateMachine<EitherShift, Initial> {
    fn from(val: StateMachine<Shift, PostEnter>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: Initial,
        }
    }
}

impl From<StateMachine<Shift, PostEnter>> for StateMachine<EitherShift, PostTilde> {
    fn from(val: StateMachine<Shift, PostEnter>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: PostTilde,
        }
    }
}

impl From<StateMachine<EitherShift, PostTilde>> for StateMachine<EitherShift, Initial> {
    fn from(val: StateMachine<EitherShift, PostTilde>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: Initial,
        }
    }
}

impl From<StateMachine<EitherShift, PostTilde>> for StateMachine<EitherShift, PostEnter> {
    fn from(val: StateMachine<EitherShift, PostTilde>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: PostEnter,
        }
    }
}

impl From<StateMachine<EitherShift, PostTilde>> for StateMachine<EitherShift, Exit> {
    fn from(val: StateMachine<EitherShift, PostTilde>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: Exit,
        }
    }
}

impl From<(StateMachine<EitherShift, PostTilde>, i32)> for StateMachine<EitherShift, SwitchTo> {
    fn from(val: (StateMachine<EitherShift, PostTilde>, i32)) -> Self {
        StateMachine {
            keyboard_state: val.0.keyboard_state,
            shift: EitherShift,
            state: SwitchTo(val.1),
        }
    }
}

impl From<StateMachine<EitherShift, SwitchTo>> for StateMachine<EitherShift, PostEnter> {
    fn from(val: StateMachine<EitherShift, SwitchTo>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: PostEnter,
        }
    }
}

impl From<StateMachine<EitherShift, PostTilde>> for StateMachine<EitherShift, SwitchToNext> {
    fn from(val: StateMachine<EitherShift, PostTilde>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: SwitchToNext,
        }
    }
}

impl From<StateMachine<EitherShift, SwitchToNext>> for StateMachine<EitherShift, PostEnter> {
    fn from(val: StateMachine<EitherShift, SwitchToNext>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: PostEnter,
        }
    }
}

impl From<StateMachine<EitherShift, PostTilde>> for StateMachine<EitherShift, SwitchToPrevious> {
    fn from(val: StateMachine<EitherShift, PostTilde>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: SwitchToPrevious,
        }
    }
}

impl From<StateMachine<EitherShift, SwitchToPrevious>> for StateMachine<EitherShift, PostEnter> {
    fn from(val: StateMachine<EitherShift, SwitchToPrevious>) -> Self {
        StateMachine {
            keyboard_state: val.keyboard_state,
            shift: EitherShift,
            state: PostEnter,
        }
    }
}

impl Factory {
    pub fn update_key(mut self, key: u32, direction: KeyState) -> Self {
        use keycode::{KeyMap, KeyMapping::Evdev, KeyMappingId};
        use std::convert::TryInto;
        use Factory::*;
        use KeyState::*;
        eprintln!("Got key event: {:#?} {} {:?}", self, key, direction);
        let key_map = KeyMap::from_key_mapping(Evdev(key.try_into().unwrap())).unwrap();
        match &mut self {
            Initial(s) => s.keyboard_state.update_key(key_map, direction.into()),
            PostEnter(s) => s.keyboard_state.update_key(key_map, direction.into()),
            ShiftPostEnter(s) => s.keyboard_state.update_key(key_map, direction.into()),
            UnShiftPostEnter(s) => s.keyboard_state.update_key(key_map, direction.into()),
            PostTilde(s) => s.keyboard_state.update_key(key_map, direction.into()),
            Exit(s) => s.keyboard_state.update_key(key_map, direction.into()),
            SwitchTo(s) => s.keyboard_state.update_key(key_map, direction.into()),
            SwitchToNext(s) => s.keyboard_state.update_key(key_map, direction.into()),
            SwitchToPrevious(s) => s.keyboard_state.update_key(key_map, direction.into()),
        }
        match (self, key_map.id, direction) {
            (Initial(s), KeyMappingId::Enter, Released) => PostEnter(s.into()),
            (Initial(s), _, _) => Initial(s),
            (PostEnter(s), KeyMappingId::Enter, _) => PostEnter(s),
            (PostEnter(s), KeyMappingId::ShiftLeft, Pressed) => ShiftPostEnter(s.into()),
            (PostEnter(s), KeyMappingId::ShiftRight, Pressed) => ShiftPostEnter(s.into()),
            (ShiftPostEnter(s), KeyMappingId::Enter, _) => ShiftPostEnter(s),
            (UnShiftPostEnter(s), KeyMappingId::ShiftLeft, Pressed) => ShiftPostEnter(s.into()),
            (UnShiftPostEnter(s), KeyMappingId::ShiftRight, Pressed) => ShiftPostEnter(s.into()),
            (UnShiftPostEnter(s), KeyMappingId::Enter, _) => UnShiftPostEnter(s),
            (PostEnter(s), KeyMappingId::ShiftLeft, Released) => UnShiftPostEnter(s.into()),
            (PostEnter(s), KeyMappingId::ShiftRight, Released) => UnShiftPostEnter(s.into()),
            (ShiftPostEnter(s), KeyMappingId::ShiftLeft, Released) => UnShiftPostEnter(s.into()),
            (ShiftPostEnter(s), KeyMappingId::ShiftRight, Released) => UnShiftPostEnter(s.into()),
            (ShiftPostEnter(s), KeyMappingId::Backquote, Released) => PostTilde(s.into()),
            (PostTilde(s), KeyMappingId::Period, Released) => Exit(s.into()),
            (PostTilde(s), KeyMappingId::F1, Released) => SwitchTo((s, 1).into()),
            (PostTilde(s), KeyMappingId::F2, Released) => SwitchTo((s, 2).into()),
            (PostTilde(s), KeyMappingId::F3, Released) => SwitchTo((s, 3).into()),
            (PostTilde(s), KeyMappingId::F4, Released) => SwitchTo((s, 4).into()),
            (PostTilde(s), KeyMappingId::F5, Released) => SwitchTo((s, 5).into()),
            (PostTilde(s), KeyMappingId::F6, Released) => SwitchTo((s, 6).into()),
            (PostTilde(s), KeyMappingId::F7, Released) => SwitchTo((s, 7).into()),
            (PostTilde(s), KeyMappingId::F8, Released) => SwitchTo((s, 8).into()),
            (PostTilde(s), KeyMappingId::F9, Released) => SwitchTo((s, 9).into()),
            (PostTilde(s), KeyMappingId::F10, Released) => SwitchTo((s, 10).into()),
            (PostTilde(s), KeyMappingId::F11, Released) => SwitchTo((s, 11).into()),
            (PostTilde(s), KeyMappingId::F12, Released) => SwitchTo((s, 12).into()),
            (PostTilde(s), KeyMappingId::F13, Released) => SwitchTo((s, 13).into()),
            (PostTilde(s), KeyMappingId::F14, Released) => SwitchTo((s, 14).into()),
            (PostTilde(s), KeyMappingId::F15, Released) => SwitchTo((s, 15).into()),
            (PostTilde(s), KeyMappingId::F16, Released) => SwitchTo((s, 16).into()),
            (PostTilde(s), KeyMappingId::F17, Released) => SwitchTo((s, 17).into()),
            (PostTilde(s), KeyMappingId::F18, Released) => SwitchTo((s, 18).into()),
            (PostTilde(s), KeyMappingId::F19, Released) => SwitchTo((s, 19).into()),
            (PostTilde(s), KeyMappingId::F20, Released) => SwitchTo((s, 20).into()),
            (PostTilde(s), KeyMappingId::F21, Released) => SwitchTo((s, 21).into()),
            (PostTilde(s), KeyMappingId::F22, Released) => SwitchTo((s, 22).into()),
            (PostTilde(s), KeyMappingId::F23, Released) => SwitchTo((s, 23).into()),
            (PostTilde(s), KeyMappingId::F24, Released) => SwitchTo((s, 24).into()),
            (PostTilde(s), KeyMappingId::ArrowRight, Released) => SwitchToNext(s.into()),
            (PostTilde(s), KeyMappingId::ArrowLeft, Released) => SwitchToPrevious(s.into()),
            (x, _, _) => x.reset(),
        }
    }

    pub fn latch(self) -> Self {
        match self {
            Factory::SwitchTo(s) => Factory::PostEnter(s.into()),
            Factory::SwitchToNext(s) => Factory::PostEnter(s.into()),
            Factory::SwitchToPrevious(s) => Factory::PostEnter(s.into()),
            _ => unimplemented!(),
        }
    }

    fn reset(self) -> Self {
        match self {
            Factory::PostEnter(s) => Factory::Initial(s.into()),
            Factory::ShiftPostEnter(s) => Factory::Initial(s.into()),
            Factory::UnShiftPostEnter(s) => Factory::Initial(s.into()),
            Factory::PostTilde(s) => Factory::Initial(s.into()),
            _ => unimplemented!(),
        }
    }
}
