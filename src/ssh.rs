use hmacsha1::hmac_sha1;
use thrussh::*;
use thrussh_keys::*;

#[derive(Clone)]
pub struct Client {
    pub host: String,
    pub host_port: String,
}

impl client::Handler for Client {
    type Error = anyhow::Error;
    type FutureUnit = futures::future::Ready<Result<(Self, client::Session), anyhow::Error>>;
    type FutureBool = futures::future::Ready<Result<(Self, bool), anyhow::Error>>;

    fn finished_bool(self, b: bool) -> Self::FutureBool {
        futures::future::ready(Ok((self, b)))
    }
    fn finished(self, session: client::Session) -> Self::FutureUnit {
        futures::future::ready(Ok((self, session)))
    }
    fn check_server_key(self, have_pubkey: &key::PublicKey) -> Self::FutureBool {
        use std::io;
        let known_hosts = {
            use std::io::{BufRead, BufReader};
            let file =
                std::path::Path::new(&std::env::var("HOME").unwrap()).join(".ssh/known_hosts");
            BufReader::new(std::fs::File::open(file).unwrap()).lines()
        };
        if !
            known_hosts
                .enumerate()
                .filter_map(|(line_number, entry_result)| {
                    let line_number = line_number+1;
                    entry_result
                        .map(|entry| {
                            let mut entry = entry.split_ascii_whitespace();
                            match (entry.next(), entry.next(), entry.next()) {
                                (None, _, _) => None,
                                (Some(first_word), _, _) if first_word.starts_with('#') => None,
                                (Some(hosts_hash), Some(algo), Some(pubkey))
                                    if hosts_hash.starts_with("|1|") =>
                                {
                                    use base64::decode;
                                    let mut parts =
                                        hosts_hash.trim_start_matches("|1|").splitn(2, '|');
                                    match (parts.next(), parts.next(), parts.next()) {
                                        (Some(salt), Some(hash), None) => {
                                            if let (Ok(salt), Ok(hash)) = (decode(salt),decode(hash)){
                                                if let Ok(pubkey) = key::PublicKey::parse(algo.as_bytes(), pubkey.as_bytes()) { Some((
                                            line_number,
                                            Result::<_, (String, key::PublicKey)>::Ok((
                                                salt,
                                                hash,
                                                pubkey,
                                            )),
                                        ))
                                     } else {

                                    None
                                    }
                                    } else {eprintln!("Failed to decode one or more base64 in known_hosts: {}", line_number); None}},
                                        (None, _, _) => {
                                            eprintln!("Lacks known_hosts hash: {}", line_number);
                                            None
                                        }
                                        (_, None, _) => {
                                            eprintln!(
                                                "Missing host hash in known_hosts: {}",
                                                line_number
                                            );
                                            None
                                        }
                                        (_, _, Some(_)) => {
                                            eprintln!(
                                                "Unexpected '|' in known_hosts: {}",
                                                line_number
                                            );
                                            None
                                        }
                                    }
                                }
                                (Some(first_word), _, _) if first_word.starts_with('|') => {
                                    eprintln!("Unknown known_hosts hash method: {}", line_number);
                                    None
                                }
                                (Some(hosts_list), Some(algo), Some(pubkey)) => {
                                    if let Ok(pubkey) = key::PublicKey::parse(algo.as_bytes(), pubkey.as_bytes()) {
                                    Some((line_number, Err((hosts_list.to_string(), pubkey)))) } else {

                                    None
                                    }
                                }
                               (Some(_), None, _)| (Some(_), _, None) => {
                                    eprintln!("Missing known_hosts public key: {}", line_number);
                                    None
                                }
                            }
                        })
                        .transpose()
                })
                .filter_map(|entry_result| entry_result
                    .map(|(line_number, entry)| match entry {
                        Ok((salt, hash, want_pubkey)) => {
                            if ![&self.host, &self.host_port].iter().any(|x| hash == hmac_sha1(&salt, x.as_bytes())) {return None}
                               if have_pubkey == &want_pubkey {
                                  Some(()) } else {
                                      assert_eq!(have_pubkey,&want_pubkey);
                                   unimplemented!( "pubkey dosn't match known_hosts: {}", line_number)
                                }
                        }
                        Err((hosts_list, want_pubkey)) => {
                            if ![&self.host, &self.host_port].iter().any(|x|hosts_list
                                .split(',')
                                .any(|host|  &host == x))
                            { return None}
                               if have_pubkey == &want_pubkey {
                                  Some(()) } else {
                                  unimplemented!("pubkey dosn't match known_hosts: {}", line_number)
                                }
                        }
                    }).transpose()).any(|x|x.is_ok())
        {
                                    eprintln!("Can't parse public keys.");
        }

        #[cfg(feature = "bob")]
        if !known_hosts
            .map(Result::<_, _>::unwrap)
            .enumerate()
            .filter(|(line_number, x)| {
                let mut x = x.chars().skip_while(|&x| x == ' ' || x == '\t').peekable();
                match x.peek() {
                    None => unreachable!("Line can't end here"),
                    Some('\n') | Some('#') => false,
                    Some(&c) => {
                        let mut x = x.take_while(|&x| x != ' ' && x != '\t');
                        if c == '|' {
                            assert_eq!(x.next(), Some('|'));
                            if x.next() != Some('1') || x.next() != Some('|') {
                                /* This is both unimplemented!() and only a warning. */
                                eprintln!("Unknown known_hosts hash method line: {}", line_number);
                                false
                            } else {
                                let (_, salt, hash) = x.fold(
                                    (true, "".to_string(), "".to_string()),
                                    |(on_salt, mut salt, mut hash), x| {
                                        if x == '|' {
                                            assert!(on_salt);
                                            return (false, salt, hash);
                                        }
                                        if on_salt { salt } else { hash }.push(x);
                                        (on_salt, salt, hash)
                                    },
                                );
                                true
                            }
                        } else {
                            x.clone().eq(self.host.chars()) || x.eq(self.host_port.chars())
                        }
                    }
                }
            })
            .filter(|x| pubkey.is_match(x))
            .any(|_x| true)
        {
            panic!("Failed host key check")
        }
        self.finished_bool(true)
    }
    fn channel_open_confirmation(
        self,
        channel: ChannelId,
        _max_packet_size: u32,
        _window_size: u32,
        session: client::Session,
    ) -> Self::FutureUnit {
        println!("channel_open_confirmation: {:?}", channel);
        self.finished(session)
    }
    fn data(self, channel: ChannelId, data: &[u8], session: client::Session) -> Self::FutureUnit {
        println!(
            "data on channel {:?}: {:?}",
            channel,
            std::str::from_utf8(data)
        );
        self.finished(session)
    }
}
