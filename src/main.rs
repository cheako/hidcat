use std::sync::Arc;

use clap::{App, Arg};

#[cfg(feature = "full")]
mod state;
use futures::StreamExt;
mod ssh;
use ssh::*;

/*
fn blocking_call<'d, 'p, 'i, 'm, D, P, I, M>(
    conn: &DBusConnection,
    destination: D,
    path: P,
    interface: I,
    method: M,
    arguments: Option<Vec<MessageItem>>,
) -> Result<Message, Error>
where
    D: Into<BusName<'d>>,
    P: Into<DbusPath<'p>>,
    I: Into<Interface<'i>>,
    M: Into<Member<'m>>,
{
    let destination = destination.into().into_static();
    let path = path.into().into_static();
    let interface = interface.into().into_static();
    let method = method.into().into_static();

    let mut message = Message::method_call(&destination, &path, &interface, &method);

    if let Some(arguments) = arguments {
        message.append_items(&arguments)
    };

    let mut message = conn
        .channel()
        .send_with_reply_and_block(message, std::time::Duration::from_millis(1000))
        .map_err(|source| Error::FailedToSendDbusCall {
            bus: destination.clone(),
            path: path.clone(),
            interface: interface.clone(),
            member: method.clone(),
            source,
        })?;

    match message.as_result() {
        Ok(_) => Ok(message),
        Err(err) => Err(Error::DbusCallFailed {
            bus: destination.clone(),
            path: path.clone(),
            interface: interface.clone(),
            member: method.clone(),
            source: err,
        }),
    }
}
*/

#[tokio::main]
async fn main() {
    let res = App::new("hidcat: Cat your hides")
        .version("0.0.0")
        .author("Mike Mestnik <cheako+rust_source@mikemestnik.net>")
        .about("Parysite remote hosts")
        .arg(
            Arg::with_name("opt")
                .index(1)
                .takes_value(true)
                .multiple(true)
                .require_delimiter(true)
                .value_delimiter("@")
                .min_values(1)
                .max_values(2)
                .value_names(&["USER", "HOST[:PORT]"]),
        )
        .get_matches();

    let res = &res.args["opt"].vals;
    let config = Arc::new(thrussh::client::Config::default());
    let mut agent = thrussh_keys::agent::client::AgentClient::connect_env()
        .await
        .unwrap();

    let (user, host) = if res.len() == 1 {
        (std::env::var("USER").unwrap(), res[0].to_str().unwrap())
    } else {
        (
            res[0].clone().into_string().unwrap(),
            res[1].to_str().unwrap(),
        )
    };
    let mut session = {
        //   use futures::stream::StreamExt;
        use std::net::ToSocketAddrs;
        ::futures::stream::iter(
            host.to_socket_addrs()
                .or_else(|_| (host, 22).to_socket_addrs())
                .unwrap(),
        )
    }
    .filter_map(move |socket_addr| {
        let (host, host_port) = {
            match host.chars().position(|x| x == ':') {
                Some(x) => (host.chars().take(x).collect(), host.to_string()),
                None => (host.to_string(), format!("{}:22", host)),
            }
        };
        let config = config.clone();
        async move {
            thrussh::client::connect(config.clone(), socket_addr, Client { host, host_port })
                .await
                .ok()
        }
    })
    .take(1)
    .collect::<Vec<_>>()
    .await
    .swap_remove(0);
    if session
        .authenticate_future(
            user,
            agent.request_identities().await.unwrap().swap_remove(0),
            agent,
        )
        .await
        .1
        .unwrap()
    {
        let mut channel = session.channel_open_session().await.unwrap();
        channel.data(&b"Hello, world!"[..]).await.unwrap();
        if let Some(msg) = channel.wait().await {
            println!("{:?}", msg)
        }
    }

    #[cfg(feature = "full")]
    {
        use state::*;
        let (session, notifier) =
            smithay::backend::session::logind::LogindSession::new(None).unwrap();
        let session = smithay::backend::libinput::LibinputSessionInterface::from(session);
        let mut input = input::Libinput::new_with_udev(session);
        input.udev_assign_seat("seat0").unwrap();

        let _signal_token = notifier.signaler().register(|x| eprintln!("{:#?}", x));
        // input.as_raw_fd();

        let mut state = Factory::default();
        while let Ok(()) = input.dispatch() {
            for event in &mut input {
                use input::{event::KeyboardEvent, Event};
                match event {
                    Event::Keyboard(KeyboardEvent::Key(k)) => {
                        use input::event::keyboard::KeyboardEventTrait;
                        state = state.update_key(k.key(), k.key_state().into());
                    }
                    e => eprintln!("Got event: {:#?}", e),
                }
            }
            match state {
                Factory::Exit(_) => {
                    break;
                }
                Factory::SwitchTo(StateMachine {
                    state: SwitchTo(x), ..
                }) => {
                    use smithay::backend::session::Session;
                    notifier.session().change_vt(x).unwrap();
                    state = state.latch();
                }
                Factory::SwitchToNext(_) => {
                    /* blocking_call(
                        notifier.session().conn.borrow_mut(),
                        "org.freedesktop.login1",
                        "/org/freedesktop/login1/seat/self",
                        "org.freedesktop.login1.Seat",
                        "SwitchToNext",
                        None,
                    ) */
                    state = state.latch()
                }
                Factory::SwitchToPrevious(_) => {
                    /* blocking_call(
                        notifier.session().conn.borrow_mut(),
                        "org.freedesktop.login1",
                        "/org/freedesktop/login1/seat/self",
                        "org.freedesktop.login1.Seat",
                        "SwitchToPrevious",
                        None,
                    ) */
                    state = state.latch()
                }
                _ => {}
            }
        }
    }
}
